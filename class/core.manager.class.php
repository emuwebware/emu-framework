<?php

class emuManager
{
    protected $emuApp;

    protected $AJAXMethods = array();

    public function __construct( $emuApp )
    {
        $this->emuApp = $emuApp;

        add_action( 'init', array( $this, 'registerClasses' ), 1 );
        add_action( 'init', array( $this, 'wpinit' ), 1 );
        add_action( 'init', array( $this, 'registerModels' ), 1 );
        add_action( 'init', array( $this, 'registerCustomPostTypes' ), 1 );
        add_action( 'init', array( $this, 'registerAdminPages' ), 2 );
        add_action( 'init', array( $this, 'registerViews' ), 2 );
        add_action( 'init', array( $this, 'addShortCodes' ), 2 );
        add_action( 'init', array( $this, 'loadStyles' ), 2 );
        add_action( 'init', array( $this, 'loadScripts' ), 2 );
        add_action( 'init', array( $this, 'registerSideBars' ) );

        add_action( 'wp',   array( $this, 'afterPostSetup' ) );
        add_action( 'after_setup_theme', array( $this, 'registerMenus' ) );
        add_action( 'admin_menu', array( $this, 'registerMetaBoxes' ) );
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array($this,'admin_enqueue_scripts'));

        // Depracated - use the registerAJAXMethod and registerProcessorClass and registerProcessorMethod
        ////////////////////////////////////////////////////////////////////
        add_action( 'init', array( $this, 'processAjax' ), 3 );
        add_action( 'template_redirect', array( $this, 'processForm' ), 4 );
        ////////////////////////////////////////////////////////////////////

        add_action( 'init', array( $this, 'doAJAXMethods' ), 3 );

        $this->init();
    }

    public function doAJAXMethods()
    {
        if( !$this->emuApp->requestVars($plugin, $action) ) return;

        if( !isset( $this->AJAXMethods[$action] ) ) return;

        $ajax_action = $this->AJAXMethods[$action];

        call_user_func( $ajax_action );

        exit();
    }

    // Depracated - use registerAJAXMethod
    ////////////////////////////////////////////////////////////////
    public function registerAjaxAction( $action, $function_to_call )
    {
        $this->registerAJAXMethod( $action, $function_to_call );
    }
    ////////////////////////////////////////////////////////////////

    public function registerAJAXMethod( $action, $method_to_call )
    {
        $this->AJAXMethods[ $action ] = $method_to_call;
    }

    // Depracated - use $emuApp->registerProcessor
    ////////////////////////////////////////////////////////////////
    public function registerProcessorClass( $action, $class = '', $filename = '', $reqs = array('emuProcessor'), $path = null )
    {
        $this->emuApp->registerProcessor( $action, $class = '', $filename = '', $reqs = array('emuProcessor'), $path = null );
    }

    // Depracated - Alias of registerProcessorClass
    public function registerProcessor( $action, $class = '', $filename = '', $reqs = array('emuProcessor'), $path = null )
    {
        $this->registerProcessorClass( $action, $class, $filename, $reqs, $path );
    }

    // Depracated - use registerProcessorMethod
    ////////////////////////////////////////////////////////////////
    public function registerProcessorFunction( $action, $function_to_call )
    {
        $this->registerProcessorMethod( $action, $function_to_call );
    }
    ////////////////////////////////////////////////////////////////

    public function registerProcessorMethod( $action, $method_to_call )
    {
        $this->emuApp->registerProcessorMethod($action, $method_to_call);
    }

    function nonce( $args = array() )
    {
        extract( wp_parse_args( $args, array(
            'action' => 'setopts'
            ,'name' => get_class($this).'_nonce'
            ,'referer' => false // the referrer thing is pretty much deprecated, dont bother.
            ,'echo' => true
            ) ) );
        wp_nonce_field($action, $name, $referer, $echo );
    }

    function checknonce( $args = array() )
    {
        extract( wp_parse_args( $args, array(
            'action' => 'setopts'
            ,'name' => get_class($this).'_nonce'
            ) ) );
        return wp_verify_nonce($_POST[$name],$action); //yes, the order of these are reversed from wp_nonce_field()
    }

    public function init()
    {}

    public function wpinit()
    {}

    public function addShortCodes()
    {}

    public function registerAdminPages()
    {}

    public function registerViews()
    {}

    public function registerClasses()
    {}

    public function registerModels()
    {}

    public function processForm()
    {}

    public function loadStyles()
    {}

    public function loadScripts()
    {}

    public function registerCustomPostTypes()
    {}

    public function registerMetaBoxes()
    {}
	public function admin_menu(){}//same hook as registermetaboxes...(admin_menu) just more generically named i guess.
	public function admin_enqueue_scripts(){}

    public function registerSideBars()
    {}

    public function registerMenus()
    {}

    public function install()
    {}

    public function afterPostSetup()
    {}

    // Depracated - use registerAJAXMethod
    ////////////////////////////////////////////////////////////////
    public function processAjax()
    {}
    ////////////////////////////////////////////////////////////////
}

?>